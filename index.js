const express = require("express");
var path = require("path");
const app = express();
app.use(express.static("public"));
app.use(express.json());
const port = 3000;

app.get("/", (request, response) => {
  response.redirect("/home");
});

app.get("/home", (request, response) => {
  response.sendFile(__dirname + "/views/home.html");
});

app.get("/game", (request, response) => {
  response.sendFile(__dirname + "/views/game.html");
});

app.get("/login", (request, response) => {
  response.sendFile(__dirname + "/views/login.html");
});

app.post("/auth/login", (req, res) => {
  const users = [
    {
      username: "wahyu",
      password: "1234",
    },
    {
      username: "test",
      password: "1234",
    },
  ];
  const { username, password } = req.body;

  console.log({ username, password });

  const findUser = users.find((user) => {
    return user.username === username;
  });

  if (findUser) {
    if (findUser.password === password) {
      res.status(200).json({ success: true, message: "Login Berhasil" });
    } else {
      res.status(400).json({ success: false, message: "Login Gagal, password salah" });
    }
  } else {
    res.status(400).json({ success: false, message: "Login Gagal, user tidak ditemukan" });
  }
});

app.listen(port, () => {
  console.log(`cli-nodejs-api listening at http://localhost:${port}`);
});
